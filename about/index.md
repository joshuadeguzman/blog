---
layout: page
current: about
title: About
navigation: true
logo: 'assets/images/ghost.png'
class: page-template
subclass: 'post page'
---

Joshua is an enthusiastic software engineer. 
Experienced with industry grade software project management and development and systems software integration.
Have strong problem solving and critical thinking skills.
Have strong work ethic, team oriented, flexible and dependable.

Worked with contract based projects for almost 2 years when I was in college both for local and abroad based clients.
Already worked for some contract based projects alone, but as always, works best with a team.

> I volunteer for various communities that promote causes for immersive technologies like AR/VR, blockchain and artificial intelligence which I believe will be few of the once in a life time inventions to be embraced by mankind today and in the succeeding years to come.

More about Joshua's work portfolio, do visit
[www.devjdg.com](https://jodeio.github.io/).
